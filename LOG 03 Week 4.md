**LOGBOOK WEEK 4(FRIDAY, 12/11/2021)**

**Logbook Entry Number : 03** 

**Group : Team 7**

**Subsystem : Design and Structure**

****
**Task 1 : Subsystem's presentation in the class (Friday, 05/11/2021)**

1. Presented the plans and updates of the tasks of our ideas until the end of the semester.

**Task 2 : Meeting with the Team (Friday, 05/11/2021)**
  
1. Updated the details of the respective subsystem.
2. Appointed roles.
3. Decided on how to write the logbook.

**Task 3 : Subsystem Visit to the Lab (Tuesday, 09/11/2021)**

1. Visit was at 10 am and hence I weren't able to join the subsytem's lab visit to check on gondola.

**Task 4 : Final Decision (Thursday, 11/11/2021)**

1. Decided on the suitable shape, size and material of the gondola.
 
 ![Design](https://gitlab.com/putraspace/idp-2021-group/design-and-structure/-/blob/main/Gondola/WhatsApp_Image_2021-11-11_at_11.08.18_PM.jpeg)
 


