**LOGBOOK WEEK 8 (FRIDAY, 17/12/2021)**

**Logbook Entry Number : 07** 


**Group : Team 7**

**Subsystem : Design and Structure**

****
**Task 1 : Subsystem's presentation in the class (Friday, 26/11/2021)**

1. Presented the idea of V shaped botom layered gondola design. 

  ![Gondola design V shaped bottom layer](https://gitlab.com/JameelahSuhaila/my-idp-project/-/blob/main/40C24F19-9A2D-424C-85BF-F593F3324B0A.jpeg)


**Task 2 : Meeting with the Team (Friday, 03/12/2021)**
  
1. Checked the logbok submission.
2. No role was taken by me.


**Task 3 : Meeting with the subsystem (Friday, 03/12/2021)**

1. Estimated and calculated the weight of the gondola components.

![Gondola components weight spreadsheet](https://gitlab.com/JameelahSuhaila/my-idp-project/-/blob/main/Screenshot__193_.png)

**Task 4 : 3D printing of the gondola with the subsystem (Monday, 13/12/2021)**

1. 3D printed the gondola in Mr Azizi Malek's office and then attched the latches on every side of the gondola.
2. Final design was printed using the plywood for the first flight test since using the ABS Filament would take a long time to print and hencethe gondola made from ABS Filament would be used for the second flight test.

![3D printed Gondola design bottom layer](https://gitlab.com/JameelahSuhaila/my-idp-project/-/blob/main/3D_printed_bottom_layer.jpeg)

![3D printed complete gondola design](https://gitlab.com/JameelahSuhaila/my-idp-project/-/blob/main/3D_Printed_gondola.jpeg)



