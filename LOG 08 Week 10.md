**LOGBOOK WEEK 10 (FRIDAY, 07/01/2022)**

**Logbook Entry Number : 08** 


**Group : Team 7**

**Subsystem : Design and Structure**

****
**Task 1 : Meeting with the other subsystems (Tuesday, 04/01/2022)**

1. It was suggested to modify the gondola design and make it compact by resizing.
2. Gondola would be placed on top of the tank attached together with the velcro.
3. The material used must be some stronger wood. 
4. Exact dimesions for gnss/telemetry pocket must be measured.
5. Changes must be done with the carried board holder.

**Task 2 : Meeting with the subsytem(Thursday, 06/01/2022)**
1. Planned on how the changes must be carried out by considering exact measurements of the components placed inside the gondola.
2. Solid vents would be made just like the bottom layer.
3. Sleeves would be attached at the top layer to insert esc.
4. Mass of the GNSS measured was 48g and the mass of the telemetry is 27g.
5. Gondola resizing measurements were done.
6. Two new sprayer components relay switch for pump and wifi module were introduced.
7. If possible, relay switch for pump would be placed in the bottom layer.
8. Wifi module would be placed on or near the carrier board.
9. The ziptie would be used on the corners of the carrier board holder.
10. The external battery for the sprayer was measured to be 307g. The battery size is 5.5cm x 6.6cm x 3.6 cm.

![resizing ideas 1](https://gitlab.com/JameelahSuhaila/my-idp-project/-/blob/main/Gondola%20resizing/IMG_0523.PNG)

![resizing ideas 2](https://gitlab.com/JameelahSuhaila/my-idp-project/-/blob/main/Gondola%20resizing/IMG_0524.PNG)

![resizing ideas 3](https://gitlab.com/JameelahSuhaila/my-idp-project/-/blob/main/Gondola%20resizing/IMG_0525.PNG)

![external battery weight](https://gitlab.com/JameelahSuhaila/my-idp-project/-/blob/main/Gondola%20resizing/WhatsApp_Image_2022-01-07_at_12.20.21_PM.jpeg)

