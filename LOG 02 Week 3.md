**LOGBOOK WEEK 3(SUNDAY, 07/11/2021)**

**Logbook Entry Number : 02** 


**Group : Team 7**

**Subsystem : Design and Structure**

****
**Task 1 : Checking Materials and Equipments in the Lab**

  **Update :** 
1. The thruster arm works, but must try to design a more stable form. Some of the components needs to be fabricated.
2. Gondola must be designed and fabricated using 3D printer. Shape must be optimized and Box must be designed with 3   layers with the first layer consisting of ESC, the second layer should be that of the control system and the final third layer must have the distributor load. 

**Task 2 : Subsystem Group Discussion on Discord (Friday, 30/10/2021)**

1. Divided the design subsytem into 2 groups : thruster arm and gondola groups.
2. Planned to have discussion with the Control system team for size, shape and suitable material of distributor load cover.
3. Planned to have discussion with the Payload team regarding the mounting mechanism attached to the sprayer.

**Task 3 : Meeting with the Team (Saturday, 31/10/2021)**

1. Updated the team members about the discussions made in the respective subsystems.
 

**Task 4 : Zoom Meeting with Amirul Fikri (Wednesday, 03/11/2021)**

1. Could visually see the old sample thruster arm as well as the gondola for those who could not visit the lab.
2. Elaborated on the layering part of the gondola.
