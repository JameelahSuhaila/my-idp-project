**LOGBOOK WEEK 6(FRIDAY, 26/11/2021)**

**Logbook Entry Number : 05** 


**Group : Team 7**

**Subsystem : Design and Structure**

****
**Task 1 : Subsystem's presentation in the class (Friday, 19/11/2021)**

1. Presented the idea of new Gondola mateterial(mppf), layering method(stacked layers) and the position of lightening holes(at the base).

![Design](https://gitlab.com/putraspace/idp-2021-group/design-and-structure/-/blob/main/Gondola/2nd_Design.jpeg)

**Task 2 : Meeting with the Team (Friday, 19/11/2021)**
  
1. Updated the details of the respective subsystem.
2. Appointed Roles.
3. Took up the role of the Recorder.


**Task 3 : Lab Visit with the Subsystem (Thursday, 25/11/2021)**

1. Discussed about the sprayer attachment to the gondola and where to place the cropsprayer fuel tank with the Payload subsystem. 
2. Took measurements of old gondola models.
3. Discussed with Fikri and received some suggestions such as to make the third layer of the gondola hollow, 3D print the gondola in parts since the printer in the lab can only take up to 10cm x 10cm dimensions and then glue them together with ABS Resin. 
