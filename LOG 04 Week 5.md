**LOGBOOK WEEK 5(FRIDAY, 19/11/2021)**

**Logbook Entry Number : 04** 


**Group : Team 7**

**Subsystem : Design and Structure**

****
**Task 1 : Subsystem's presentation in the class (Friday, 12/11/2021)**

1. Presented the Gondola Design idea.
2. Fikri and Azizi suggested to change idea of using the sliding layers and strawbox material into something lighter such as the carbon fibre composite.

![Design](https://gitlab.com/putraspace/idp-2021-group/design-and-structure/-/blob/main/Gondola/WhatsApp_Image_2021-11-11_at_11.08.18_PM.jpeg)

**Task 2 : Meeting with the Team (Friday, 12/11/2021)**
  
1. Updated the details of the respective subsystem.
2. Appointed Roles.
3. Took up the role of the Co ordinator.


**Task 3 : Updates about the Subsystem (Thursday, 18/11/2021)**

1. Decided on the outer frame of gondola with stacked layers and lightning holes at the base in order to connect wires in between the layers.

![Gondola Design](https://gitlab.com/putraspace/idp-2021-group/design-and-structure/-/blob/main/Gondola/2nd_Design.jpeg)
 
 


