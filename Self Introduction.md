**Self Introduction**
> Hello, I am Jameelah Shuhaila from India.

![aircraft](https://images.unsplash.com/photo-1520437358207-323b43b50729?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YWlyY3JhZnR8ZW58MHx8MHx8&ixlib=rb-1.2.1&w=1000&q=80)

| Strength | Weakness |
| ------ | ------ |
| `Teamwork`| `Unable to maintain the balance ` |
|`flexibility` |`Overthinking`|

**Experiencing Gitlab**

- Creating the markdown file using **_Gitlab_** wasn't as difficult as I thought it would be.
- I would like to try out the numbered list although I have no texts to add there. So I would be using random texts.
1. Aerospace
2. Boeing

[Adding link-trial](https://www.google.com/search?q=aircraft&tbm=isch&ved=2ahUKEwj5gsWA0ezzAhVmyXMBHYAaCEUQ2-cCegQIABAA&oq=aircraft&gs_lcp=CgNpbWcQAzIHCAAQsQMQQzIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDoHCCMQ7wMQJzoECAAQQzoKCCMQ7wMQ6gIQJzoICAAQgAQQsQNQ1cMBWIfeAWCa4wFoAXAAeASAAbMDiAGGGJIBCTQuNC4xLjIuM5gBAKABAaoBC2d3cy13aXotaW1nsAEHwAEB&sclient=img&ei=61d6YbnhLuaSz7sPgLWgqAQ)

- [ ] 
 **Introduce yourself**
- [ ] 
**Add Image**
- [ ] 
**Create the table**
- [ ] 
**Create lists**
- [ ] 
**Try out the code**

**Python Code**

<details><summary>Click to expand </summary>


**Trying out some python coding**

`print ("hello world")

a=float(input("Enter the value of a"))

b=float(input("Enter the value of b")

c = (a^2) + (b^3)         

Note : [Replace ^ with **]

print('c')`
