**LOGBOOK WEEK 11 (FRIDAY, 14/01/2022)**

**Logbook Entry Number : 09** 


**Group : Team 7**

**Subsystem : Design and Structure**

****
**Task 1: Resume Submission(Friday,07/01/2022)**

1. Submitted the resume in the team 7 discord channel.

**Task 2 : Lab visit with the subsytem(Wednesday, 12/01/2022)**
1. 2nd prototype of gondola was produced by laser cutting machine using plywood in Mr. Azizi Malek's office. The new design is aesthetically pleasing, is lighter and smaller.
2. The weight of the gondola after placing the control system components reduced from 3.6 kg to 1.8 kg. The ABS filament gondola as per the initial plan would have weighed 3.6 kg(estimated weight). The 1st prototype using plywood weighed around 1.9 kg and the 2nd prototype(final design) weighs approximately 1.8 kg. The time consumed to produce the plywood gondola was 1-2 days as compared to the time consumed to produce the ABS filament which is 3-4 days, thus the reason why ABS filament gondola did not come into the practical use. An excel spreadsheet has been created to calculate and compare the total weight. (Refer to the weight calculation Spreadsheet)

[Gondola design-2nd prototype](https://drive.google.com/file/d/1zwiIz7G2GwcF4UdgKa63T2pkFC4wEQh6/view?usp=sharing)

[Weight Calculation Spreadsheet](https://docs.google.com/spreadsheets/d/1zJFqVRZnqqdmaEmJhhe00Xvv7G7O-h1p1HeizUGBYRk/edit?usp=sharing)

**Task 3: Report Completion (Friday, 14/01/2022)**
1. Volunteered to write the report for the team gondola of the design subsytem.






