**LOGBOOK WEEK 7(FRIDAY, 03/12/2021)**

**Logbook Entry Number : 06** 


**Group : Team 7**

**Subsystem : Design and Structure**

****
**Task 1 : Subsystem's presentation in the class (Friday, 26/11/2021)**

1. Presented the idea of the latest Gondola design, attachment of sprayer mechanism to the gondola as well as the 3D printing ideas of the Gondola.
2. Volunteered to present.

  ![Gondola Design](https://gitlab.com/JameelahSuhaila/my-idp-project/-/blob/main/Gondola_Design_week_6.jpeg)


**Task 2 : Meeting with the Team (Friday, 26/11/2021)**
  
1. Updated the details of the respective subsystem.
2. Appointed Roles.
3. Took up the role of the Process Monitor.


**Task 3 : Meeting with the subsystem (Friday, 26/11/2021)**

1. Some design improvements were to be made with the gondola design such as the aesthetic shape, V Shaped bottom layer, replacing the x shape on the sides with the vent, adding hole at the top in order for the sppeed indicator to be exposed and to design the carrier board mount that can hold with nuts and bolts. 

**Task 4 : Final decision with the subsystem (Friday, 03/12/2021)**

1. Finalised the design.
2. Decided to use the fabrication lab 3D printer since it can print 40x40 cm while the dimesion of our gondola is 30x20 cm. 

![Gondola design bottom layer](https://gitlab.com/JameelahSuhaila/my-idp-project/-/blob/main/40C24F19-9A2D-424C-85BF-F593F3324B0A.jpeg)

