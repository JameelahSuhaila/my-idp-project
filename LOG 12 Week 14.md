**LOGBOOK WEEK 14 (FRIDAY, 28/01/2022)**

**Logbook Entry Number : 12** 


**Group : Team 7**

**Subsystem : Design and Structure**

**Task 1: Lab visit for flight test (Friday, 28/01/2022)**

1. The airship fabric was filled with helium gas, thruster arms, gondola and propellers were attached to the airship for the flight test.
2. Due to some configuration and stability problems, the airship lost balance and nearly crashed.
****
**Task 2: Class discussion (Friday, 28/01/2022)**

1. Discussions were made regarding the problems that caused the airship crash.
2. Motor and propeller configurations were discovered to be the main problems. The clockwise and anticlockwise motions of the airship were not balanced.







