**LOGBOOK WEEK 12 (FRIDAY, 21/01/2022)**

**Logbook Entry Number : 10** 


**Group : Team 7**

**Subsystem : Design and Structure**

**Task 1: Subsystem presentation(Friday, 14/01/2022)**

1. Updated the progress on discord channel.

[Gondola design-2nd prototype](https://drive.google.com/file/d/1zwiIz7G2GwcF4UdgKa63T2pkFC4wEQh6/view?usp=sharing)

[Weight Calculation Spreadsheet](https://docs.google.com/spreadsheets/d/1zJFqVRZnqqdmaEmJhhe00Xvv7G7O-h1p1HeizUGBYRk/edit?usp=sharing)

2. Volunteered to present.

****
**Task 2: Resume Submission(Friday,14/01/2022)**

1. Submitted the resume in the team 7 whatsapp group collectively with the other group members.

[Resume](https://drive.google.com/drive/folders/1JYzkD0TnzroHtPDemS26qPEjvmORnNUA)








